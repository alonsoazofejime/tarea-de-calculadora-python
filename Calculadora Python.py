#Iniciamos imprimiendo el menu
#Hecho por Alonso Azofeifa y Jeaustin Huaman

print("Bienvenido a calculadora Python")
print("Restriccion: Solo podemos hacer operaciones con 2 números")
print("1 Para Restar digita")
print("2 Para Sumar digita")
print("3 Para Multiplicar digita")
print("4 Para Dividir digita")
print("5 Para Elevar digita")
print("6 Para hacer  lograrito, nota: solo se usara el primer numero diguitado, digita")
print("7 Para division entera")

def SeleccionDeMenu():
    import math #importamos esto, para poder hacer el logaritmo de base 10
    
    seleccion = int(input("Digite la opccion para proceder "))	#La entrada para la seleccion de una opccion anterirormente impresa
    
    while (seleccion >0 and seleccion <7): #seleccione un numero que esté en el intervalo de 0 a 7
           
        numeros = input("Digite los dos numeros separados por un espacio ")  #Digite dos numeros que van a hacer separados por un espacio dependiendo de lo que este adentro del parentesis del .split
       
        array_n = numeros.split()
        PrimerNumero = int(array_n[0].strip()) #El .strip permite sacar 2 sifras en un mismo renglon
        SegundoNumero = int(array_n[1].strip()) #El .strip permite sacar 2 sifras en un mismo renglon
        
        print(numeros)
       
        # resulve la operacion de resta y pone el resultado en un str
        if (seleccion==1):
            resultado = PrimerNumero - SegundoNumero
            print("La Resta es:" + str(resultado))
            
        # resulve la operacion de suma y pone el resultado en un str      
        elif(seleccion==2):
            resultado = PrimerNumero + SegundoNumero
            print("La Suma es:" + str(resultado))
            
       # resulve la operacion de multiplicacion y pone el resultado en un str  
        elif(seleccion==3):
            resultado = PrimerNumero*SegundoNumero
            print("La Multiplicacion es:" + str(resultado))
            
        # resulve la operacion de division y pone el resultado en un str   
        elif(seleccion==4):
            resultado = PrimerNumero/SegundoNumero
            print("La Division es:" + str(resultado))

        # resulve la operacion de elevar y pone el resultado en un str    
        elif(seleccion==5):
            resultado = PrimerNumero**SegundoNumero
            print("El resultado es:" + str(resultado))

        # resulve la operacion de hacer el logaritmo y pone el resultado en un str, pero solo del PrimerNumero
        elif(seleccion==6):
            resultado = math.logy(PrimerNumero)
            print("El resultado es:" + str(resultado))

        # resulve la operacion de la division real y pone el resultado en un str
        elif(seleccion==7):
            resultado = PrimerNumero//SegundoNumero
            print("El resultado es:" + str(resultado))
            
        # reinicia la funcion para hacer otra operacion
        seleccion = int(input("Digite la opccion para proceder"))

SeleccionDeMenu()

